console.log('video6');

// Wygenerowanie tablicy asocjacyjnej z nazwą pliku jeśli jest to plik z ext = [.mp4 .ogv .wemb] 
// Przypisanie w wygenerowanej tablicy - tablicy z elementami: nazwy plików z ext [.mp4 .ogv .wemb]
var tabSource = new Array();
for(key in pliki) {

    // nazwa & ext pliku
    var imgNameExt = pliki[key];
    var imgName = imgNameExt.split('.',1); 
    //console.log(imgName);
    var extNametemp = imgNameExt.split('.',2);
    var extName = extNametemp[1];
    //console.log(extName); 
    //console.log(tabSource[imgName]);
    if(extName === 'mp4' || extName === 'ogv' || extName === 'webm') {
        //tabSource[imgName] = [];
        if(tabSource[imgName] == undefined) tabSource[imgName]= [];
        tabSource[imgName].push(imgNameExt);
    } 
}

//console.log('--- Pobrane pliki VIDEO [mp4,ogv,webm] ---');
for(video in tabSource) {
    //console.log('video='+video);
    var elementyVideo = tabSource[video];
    for(j in elementyVideo) {
        //console.log(elementyVideo[j]);
    }
}
//console.log('--- END 2 ---');

var wrapper_video = document.querySelector('#wrapper_video');

// Generowanie tagu VIDEO
for(i in tabSource) {
    var elementy = tabSource[i];

    var div_video = document.createElement('div');
    div_video.className = 'video';

    var video = document.createElement('video');
    video.setAttribute('width','300');
    video.setAttribute('height','168');
    video.id = 'player10';
    video.setAttribute('controls','controls');
    video.setAttribute('preload','none');
    
    // Dodanie atrybutu poster z odpowiednim obrazkiem do tagu VIDEO
    // Domyślny obrazek
    video.setAttribute('poster',posterDirectory+'/default/default2.jpg');
    //console.log('--- POSTER --- Wyszuka video [do 3-ch plików] i obrazek - poster  ---');
    for(var key in pliki) {
        // wszystkie pliki
        //console.log(pliki[key]);

        // nazwa & ext pliku
        var NameExt = pliki[key];
        var Name = NameExt.split('.',1); 
        //console.log(Name[0]);
        
        var ext = NameExt.split('.',2);
        //console.log(ext[1]);
        
        //console.log(elementy[0].split('.',1)[0]);
        if(Name[0] === elementy[0].split('.',1)[0]) {
            //console.log(Name[0]);
            if(ext[1] === 'jpg' || ext[1] === 'jpeg' || ext[1] === 'png' || ext[1] === 'gif' ) {
                video.setAttribute('poster',posterDirectory+NameExt);
            }
        }
    }    
    //console.log('--- POSTER end ---');
    
    video.innerHTML = "";
    for(var j in elementy) {
        // Generowanie tagu SOURCE i przypisanie mu źródła plików [.mp4 .ogv .webm]
        //console.log(elementy[j]);
        var extElement = elementy[j].split('.',2);
        //console.log(extElement[1]);
        video.innerHTML += "<source src='"+videoDirectory+elementy[j]+"' type='video/"+extElement[1]+"'/>";
    }
    var p = document.createElement('p');
    var namefile = elementy[0].split('.',1)[0];
    p.innerHTML = "Video downloaded: " + namefile;

    div_video.appendChild(video);
    div_video.appendChild(p);
    wrapper_video.appendChild(div_video); 
}


/*
            <div class="video">
                <video width="640" height="360" id="player1" poster="{{ asset('bundles/projekthtml5/video/video4/video_poster/robot.png') }}" 
                       controls="controls" preload="none">                                
                    <source src="{{ asset('bundles/projekthtml5/video/video6/robot.mp4') }}" type='video/mp4' /> 
                    <source src="{{ asset('bundles/projekthtml5/video/video6/robot.webm') }}" type='video/webm' />
                    <source src="{{ asset('bundles/projekthtml5/video/video6/robot.ogv') }}" type='video/ogg' />
                </video>
                <p>Robot - children's future toys</p>
            </div> 
 */









